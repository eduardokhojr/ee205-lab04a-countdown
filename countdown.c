///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author eduardo kho jr <eduardok@hawaii.edu>
// @date   2/10/2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

int main() {

#define SEC_YEAR 31536000
#define SEC_DAY  86400
#define SEC_HOUR 3600
#define SEC_MIN  60

unsigned int timesec, rem;

struct tm ref = {0};
	ref.tm_mday = 20;
	ref.tm_mon = 2;
	ref.tm_year = 150;

time_t hard,current,t1,t2;

time(&current);

hard = mktime(&ref);

//if(time(&current) > hard){
//timesec = (int)difftime(time(&current),hard);
//}else{
//timesec = (int)difftime(hard,time(&current));
//}

// separating the seconds into year,month, etc. // 

unsigned int years,days,hours,mins,seconds;

printf("reference time: %s",asctime(&ref));

while(1){

if(time(&current) > hard){
timesec = (int)difftime(time(&current),hard);
}else{
timesec = (int)difftime(hard,time(&current));
}

years = timesec/SEC_YEAR;
timesec = timesec - (years*SEC_YEAR);
days = timesec/SEC_DAY;
timesec = timesec - (days*SEC_DAY);
hours = timesec/SEC_HOUR;
timesec = timesec - (hours*SEC_HOUR);
mins = timesec/SEC_MIN;
timesec = timesec - (mins*SEC_MIN);
seconds = timesec;

printf("Years: %u  Days: %u  Hours: %u  Mins: %u  Seconds: %u \n",years,days,hours,mins,seconds);



sleep(1);
}
   return 0;
}
